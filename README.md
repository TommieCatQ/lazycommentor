# My project's README
AutoHotkey unleashes the full potential of your keyboard, joystick, and mouse. For example, in addition to the typical Control, Alt, and Shift modifiers, you can use the Windows key and the Capslock key as modifiers. In fact, you can make any key or mouse button act as a modifier.

Our team will use AutoHotkey to shorten some daily manual steps such as Comment in the Change, Comment in Bugzilla, Search History of Error/Crash/timed out.

More info and latest version please use this Wiki page: https://wiki.wdf.sap.corp/wiki/display/ngdb/AutoHotkey+tool