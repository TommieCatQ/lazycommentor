﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


CBug:=0
CLower:=0
CQuality:=0
CCCR:=0
CPeer:=0

::orarm::
SendInput,
(
Hi colleagues,

Test quality looks good, it is good to merge now.

Cheers,
Quan
)
Return

::oraup::
SendInput,
(
Hi colleagues,

FYI, xxxx was merged into orange.

Best regards,
Quan
)
Return

::orabf::
SendInput, 
(
The error happens reproducible in orange gerrit, so I marked below ST (s) as unstable in orange and moved them into quarantine:



Add also the keyword 2_QUARANTINE for tracking and increase priority as QUARANTINE concept. 

Best regards,
Quan
)
Return

; hotkeys for hana2sp03
::203rm::
SendInput, 
(
Hi colleagues,
- Bug approval: YES
- In orange: YES
- Quality : OK

It is good to merge now.

Cheers,
Quan
)
Return

::203up::
SendInput,
(
Hi colleagues,

FYI, xxx was merged into hana2sp03.

Best regards,
Quan
)
Return 

::203rr::
Gui, Add, Checkbox, vCBug, Bug approval
Gui, Add, Checkbox, vCLower, Available in orange
Gui, Add, Checkbox, vCQuality, Test quality

Gui, Add, Button, default, Comment_hana2sp03
Gui, Show
Return

ButtonComment_hana2sp03:
Gui, Submit, Nohide
Gui, Destroy
SendInput, Hi colleagues, {enter}

if (CBug == 1){
	SendInput, - Bug approval: YES {enter}
}

if (CLower == 1){
	SendInput,- in Orange: YES {enter}
}

if (CQuality == 1){
	SendInput, - Quality : OK {enter}
}

if (CBug&CLower&CQuality)
{
	SendInput, {enter} {enter} It's good to merge now. {enter} {enter} Cheers, {enter} Quan
}

else
{
	SendInput, {enter}Wait for:{enter}
	
	if (CBug == 0) 		{	
	SendInput, - Bug approval - please change Status into "Merge pending", then Delivery into "requested" {enter}	
	}
	if (CLower == 0) 	{	
	SendInput, - Change in orange: XXX {enter}	
	}
	if (CQuality == 0) 	{	
	SendInput, - XXX {enter}
	}
}
SendInput, {enter}Best regards, {enter}Quan
Return

::203bf::
SendInput, 
(
The error happens reproducible in hana2sp03 gerrit, so I marked below ST (s) as unstable in hana2sp03 and moved them into quarantine:



Add also the keyword 2_QUARANTINE for tracking and increase priority as QUARANTINE concept. 

Best regards,
Quan
)
Return

; hotkeys for hana2sp02
::202rm::
SendInput, 
(
Hi colleagues,
- Bug approval: YES
- In hana2sp03: YES
- Peer code review: YES
- Quality : OK
It is good to merge now.

Cheers,
Quan
)
Return

::202up::
SendInput,
(
Hi colleagues,

FYI, xxx was merged into hana2sp02.

Best regards,
Quan
)
Return 

::202rr::
Gui, Add, Checkbox, vCBug, Bug approval
Gui, Add, Checkbox, vCLower, Available in hana2sp03
Gui, Add, Checkbox, vCPeer, Peer code review
Gui, Add, Checkbox, vCQuality, Test quality

Gui, Add, Button, default, Comment_hana2sp02
Gui, Show
Return

ButtonComment_hana2sp02:
Gui, Submit, Nohide
Gui, Destroy
SendInput, Hi colleagues, {enter}

if (CBug == 1){
	SendInput, - Bug approval: YES {enter}
}

if (CLower == 1){
	SendInput,- in hana2sp03: YES {enter}
}

if (CPeer == 1){
	SendInput,- Peer code review: YES {enter}
}

if (CQuality == 1){
	SendInput, - Quality : OK {enter}
}

if (CBug&CLower&CPeer&CQuality)
{
	SendInput, {enter} {enter} It's good to merge now. {enter} {enter} Cheers, {enter} Quan
}

else
{
	SendInput, {enter}Wait for:{enter}
	
	if (CBug == 0) 		{	
	SendInput, - Bug approval - please change Status into "Merge pending", then Delivery into "requested" {enter}	
	}
	if (CLower == 0) 	{	
	SendInput, - change in hana2sp03: XXX {enter}	
	}
	if (CPeer == 0) 	{	
	SendInput, - Peer code review - please add one of your colleagues here for reviewing {enter}	
	}
	if (CQuality == 0) 	{	
	SendInput, - XXX {enter}	
	}
}
SendInput, {enter}Best regards, {enter}Quan
Return


::202bf::
SendInput, 
(
The error happens reproducible in hana2sp02 gerrit, so I marked below ST (s) as unstable in hana2sp02 and moved them into quarantine:



Add also the keyword 2_QUARANTINE for tracking and increase priority as QUARANTINE concept. 

Best regards,
Quan
)
Return

; hotkeys for hana1sp12
::112rm::
SendInput, 
(
Hi colleagues,
- Bug approval: YES
- In hana2sp02: YES
- Peer code review: YES
- Quality : OK
It is good to merge now.

Cheers,
Quan
)
Return


::112bf::
SendInput, 
(
The error happens reproducible in hana1sp12 gerrit, so I marked below ST (s) as unstable in hana1sp12 and moved them into quarantine:



Add also the keyword 2_QUARANTINE for tracking and increase priority as QUARANTINE concept. 

Best regards,
Quan
)
Return

::112abcc::
SendInput, 
(
Hi colleagues,

Next CC for REV 122.17 is scheduled on Apr 26, so could you please consider to bring the fix on hana1sp12 (and relevant branches) asap if it needed for this REV. 

Thanks and best regards,
Quan
)
Return

::112up::
SendInput,
(
Hi colleagues,

FYI, xxx was merged into hana1sp12.

Best regards,
Quan
)
Return 

::112rr::
Gui, Add, Checkbox, vCBug, Bug approval
Gui, Add, Checkbox, vCLower, Available in hana2sp02
Gui, Add, Checkbox, vCPeer, Peer code review
Gui, Add, Checkbox, vCQuality, Test quality

Gui, Add, Button, default, Comment_hana1sp12
Gui, Show
Return

ButtonComment_hana1sp12:
Gui, Submit, Nohide
Gui, Destroy
SendInput, Hi colleagues, {enter}

if (CBug == 1){
	SendInput, - Bug approval: YES {enter}
}
if (CQuality == 1){
	SendInput, - Quality : OK {enter}
}

if (CPeer == 1){
	SendInput,- Peer code review: YES {enter}
}
if (CLower == 1){
	SendInput,- in hana2sp02: YES {enter}
}


if (CBug&CLower&CPeer&CQuality)
{
	SendInput, {enter} {enter} It's good to merge now. {enter} {enter} Cheers, {enter} Quan
}

else
{
	SendInput, {enter}Wait for:{enter}
	
	if (CBug == 0) 		{	
	SendInput, - Bug approval - please change Status into "Merge pending", then Delivery into "requested" {enter}	
	}
	if (CQuality == 0) 	{	
	SendInput, - XXX {enter}	
	}
	if (CPeer == 0) 	{	
	SendInput, - Peer code review - please add one of your colleagues here for reviewing {enter}	
	}
	if (CLower == 0) 	{	
	SendInput, - change in hana2sp02: XXX {enter}	
	}
}
SendInput, {enter}Best regards, {enter}Quan
Return

; temporary hotkeys
::rjc::
SendInput, 
(
Please help to take a look and provide new PS if the failures are related to the change.

I also cancel remaining builds which are running/waiting to save the resource.

Sorry if any inconvenience caused. 
Quan
)
Return

::bugapp::
SendInput,
(
Wait for bug approval - please change Status into "Merge pending", then Delivery into "requested"
)
Return

::canflag::
SendInput,
(
Hi colleagues,

Is there any further discussion about this issue. We would like to have all the fixes related to "FAILING_CANCEL_FLAG" in gerrit finish before Friday 27th April.

Thank you for your understanding,
Quan
)
Return

::clang::
SendInput, 
(
Wait for linux64_clangasan_hm opt
{raw}Others +1
)
Return


; handling when hitting red button
GuiClose:
	Gui, Submit, Nohide
	Gui, Destroy
Return


; feature error History Hotkeys
::oraerr::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}error+only", "orange", TCName)
Return

::203err::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}error+only", "hana2sp03", TCName)
Return

::202err::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}error+only", "hana2sp02", TCName)
Return

::112err::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}error+only", "hana1sp12", TCName)
Return


; feature Crash History Hotkeys
::oracra::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}Crash", "orange", TCName)
Return

::203cra::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}Crash", "hana2sp03", TCName)
Return

::202cra::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}Crash", "hana2sp02", TCName)
Return

::112cra::
	InputBox, TCName, Test Case Name, Please enter Test Case Name, , 480, 100
	if ErrorLevel
		MsgBox, CANCEL was pressed.
	else
		Func_URLCreator("{raw}Crash", "hana1sp12", TCName)
Return

Func_URLCreator(ByRef ErrType, ByRef Branch, ByRef TC_Name)
{
	Send, {BS 5}
	SendRaw, https://maketestmonitor.wdf.sap.corp/TestCases.jsp?customInstKey=&view=result&showDetails=on&filterComponent=Engine`%2FHANA&filterReviewed=-1&filterStarted=14&sort1=MakeID&order1=asc&sort2=Status&order2=asc&showGerritInfo=on&showReviewDetails=on&filterErrors=
	SendInput, %ErrType%
	SendRaw, &filterBranch=
	SendInput, %Branch%
	SendRaw, &filterTestcase=
	SendInput, %TC_Name% 
}